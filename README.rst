======
README
======

In this project we collect info about possible technologies to use
for the Open Hardware DIN Guideline document(ation).
Partially, we also test some of those technologies.

As of right now, we have two Sphinx based projects,
one using Markdown sources,
the other using reStructuredText sources.
We store those sources in a git repository, which we host on GitLab.
We `use the GitLab built-in build service/server to compile these two projects <.gitlab-ci.yml>`_,
and then host them as the GitLab-pages associated with the repo.
These resulting static HTML based representations can be found at
`<http://osegtechdoctemplates.gitlab.io/index/>`_.

Note that the Markdown version contains mostly just a bare stub project,
while the reText version contains the actual content.
